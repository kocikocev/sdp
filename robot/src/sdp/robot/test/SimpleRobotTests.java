package sdp.robot.test;
import org.junit.*;
import org.mockito.verification.VerificationMode;

import sdp.common.RotationDirection;
import sdp.robot.I2cMotor;
import sdp.robot.Robot;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class SimpleRobotTests {
	
	I2cMotor mMotor;	
	Robot mUnderTest;
	
	@Before
	public void setUp(){
		mMotor = mock(I2cMotor.class);
		
		mUnderTest = new Robot();
		mUnderTest.initWithMotors(new I2cMotor[]{mMotor});
	}
	
	@Test
	public void move_WhenWheelAngleAndDirectionAngleSame_SpeedIsSetToMax(){
		when(mMotor.getWheelAngle()).thenReturn(0.0);
		mUnderTest.move(0);
		verify(mMotor).setSpeed(255);
	}
	
	@Test
	public void move_WhenWheelAngleAndDirectionAngleSame_SpeedIsSetToNegativeMax(){
		when(mMotor.getWheelAngle()).thenReturn(Math.PI);
		mUnderTest.move(0);
		verify(mMotor).setSpeed(-255);
	}
	
	@Test
	public void move_WhenWheelAngleAndDirection90Deg_SpeedIsSetTo0(){
		when(mMotor.getWheelAngle()).thenReturn(Math.PI/2);
		mUnderTest.move(0);
		verify(mMotor).setSpeed(0);
	}
	
	@Test
	public void rotate_WhenRotateCw_RotatesCw(){
		when(mMotor.getWheelDirectionModifier()).thenReturn(1);
		mUnderTest.rotate(RotationDirection.CLOCKWISE, 1);
		verify(mMotor).setSpeed(255);
	}
	
	@Test
	public void rotate_WhenRotateCcw_RotatesCcw(){
		when(mMotor.getWheelDirectionModifier()).thenReturn(-1);
		mUnderTest.rotate(RotationDirection.CLOCKWISE, 1);
		verify(mMotor).setSpeed(-255);
	}

}
