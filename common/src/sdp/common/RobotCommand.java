package sdp.common;

public class RobotCommand{
	public static final int MOVE = 1;
	public static final int STOP_MOVE = 2;
	public static final int STOP_ROTATE = 3;
	public static final int ROTATE_CW = 4;
	public static final int ROTATE_CCW = 5;
	public static final int KICK = 6;
	public static final int EXIT = 7;
	public static final int MAX_MOVE_SPEED = 8;
	public static final int PING = 99;
}
