package sdp.navigation.pathfinding;

import java.util.ArrayList;

public interface Pathfinder {
	public ArrayList<Node> getPath();
}
